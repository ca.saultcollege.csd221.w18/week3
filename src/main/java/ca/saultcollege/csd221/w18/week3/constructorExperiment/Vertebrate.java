/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week3.constructorExperiment;

/**
 *
 * @author rod
 */
public class Vertebrate extends Animal {
    
    private int numberOfVertebrates;
    
    public Vertebrate() {
        System.out.println("I have a backbone");
    }
    
    public Vertebrate(int numberOfVertebrates) {
        this.numberOfVertebrates = numberOfVertebrates;
    }
}
