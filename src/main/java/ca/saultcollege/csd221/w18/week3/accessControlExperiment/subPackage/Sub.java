/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week3.accessControlExperiment.subPackage;

/**
 *
 * @author rod
 */
public class Sub extends Super {
    
    public void tryAccessingStuff() {
        // From a sub class...
        
        //this.privateString = "woops";  // won't compile
        this.protectedString = "yay";    // this is ok
        this.packageString = "fine";     // this is too
        this.publicString = "fine";      // so is this
        
    }
}
