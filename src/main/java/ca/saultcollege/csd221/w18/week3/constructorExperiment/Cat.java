/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week3.constructorExperiment;

/**
 *
 * @author rod
 */
public class Cat extends Vertebrate {
    
    private int numberOfWhiskers;
    
    public Cat() {
        System.out.println("I am a cat");
    }
    
    public Cat(String sound) {
        this();
        System.out.println(sound);
    }
    
    public Cat(String sound, int numberOfWhiskers) {
        this(sound);
        this.numberOfWhiskers = numberOfWhiskers;
    }
}
