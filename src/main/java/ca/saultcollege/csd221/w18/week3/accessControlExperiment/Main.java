package ca.saultcollege.csd221.w18.week3.accessControlExperiment;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ca.saultcollege.csd221.w18.week3.accessControlExperiment.subPackage.*;
/**
 *
 * @author rod
 */
public class Main {
    
    public static void main(String[] args) {
        // From outside the package...
        
        Sub sub = new Sub();
        //sub.privateString = "woops";    // won't compile
        //sub.protectedString = "nope";   // won't compile
        //sub.packageString = "nah";      // won't compile
        sub.publicString = "yes!";        // this is fine
        
    }
    
}
