/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ca.saultcollege.csd221.w18.week3.accessControlExperiment.subPackage;

/**
 *
 * @author rod
 */
public class Super {
    private String privateString = "This is private";
    protected String protectedString = "This is private";
    String packageString = "This is private";  // default access is 'package'
    public String publicString = "This is public";
    
    public void tryAccessingStuff() {
        
        this.privateString = "we can";    // this is fine
        this.protectedString = "have";    // this is ok
        this.packageString = "it";        // this is too
        this.publicString = "all";        // so is this
    }
}
